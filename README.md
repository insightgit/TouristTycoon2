# TouristTycoon

TouristTycoon is similar to SimCity, but instead of trying to build the best City, In TouristTycoon, you build the best Tourist Trap. 
Build attractions, enact city policies, and use ads to promote your tourist trap!

This game was designed for Linux, but works on Windows & macOS. TouristTycoon was made for the Tycoon Tycoon Game Jam on [itch.io](https://itch.io/jam/jam-tycoon-tycoon-jam) 
and the [CMS Maker Faire](https://makery.pvsd.net/makerfaire/).
